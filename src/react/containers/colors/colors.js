import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {loadColorsAction} from "../../../redux/actions/colors";
import ColorsTable from "../../components/colors-table/colors-table";

class Colors extends React.Component {

    componentDidMount() {
        const {loadColors} = this.props;
        loadColors();
    }

    render() {
        const {colors, error} = this.props;
        return (
            <React.Fragment>
                <h1>Colors</h1>
                <ColorsTable colors={colors}/>
                {error && <div>{error.message}</div>}
            </React.Fragment>
        );
    }
}

Colors.propTypes = {
    loadColors: PropTypes.func.isRequired,
    colors: PropTypes.array,
    error: PropTypes.object
};

Colors.defaultProps = {
    colors: []
};

function mapStateToProps(state) {
    return {
        colors: state.colors.colors,
        error: state.colors.error
    }
}

function mapDispatchToProps(dispatch) {
    return {
        loadColors() {
            dispatch(loadColorsAction());
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Colors);
