import React from 'react';
import PropTypes from 'prop-types';

class ColorsTable extends React.Component {

    renderColors() {
        const {colors} = this.props;
        return colors.map(color => <div key={color.color}>{color.color}</div>);
    }

    render() {
        return this.renderColors();
    }
}

ColorsTable.propTypes = {
    colors: PropTypes.array
};

export default ColorsTable;
