import React from 'react';
import {Provider} from 'react-redux';
import ReactDOM from 'react-dom';
import Colors from "./react/containers/colors/colors";
import store from "./redux/store";

class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Colors/>
            </Provider>
        );
    }
}

const app = document.getElementById('app');

ReactDOM.render(<App/>, app);