import {call, put, takeEvery} from 'redux-saga/effects';
import {LOAD_COLORS} from '../action-types'
import {loadData} from '../../api';
import {loadColorsSuccessAction, loadColorsFailureAction} from '../actions/colors';


export function* loadColors() {
    try {
        const response = yield call(loadData);
        yield put(loadColorsSuccessAction(response));
    }
    catch (error) {
        yield put(loadColorsFailureAction({error}));
    }
}

export function* watchLoadColors() {
    yield takeEvery(LOAD_COLORS, loadColors)
}