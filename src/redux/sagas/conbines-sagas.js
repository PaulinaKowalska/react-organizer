import {all} from 'redux-saga/effects';
import {watchLoadColors} from './colors';

export default function* () {
    yield all([
        watchLoadColors()
    ]);
}