import {LOAD_COLORS, LOAD_COLORS_SUCCESS, LOAD_COLORS_FAILURE} from '../action-types';

export function loadColorsAction() {
    return {
        type: LOAD_COLORS
    }
}

export function loadColorsSuccessAction(payload) {
    return {
        type: LOAD_COLORS_SUCCESS,
        payload
    }
}

export function loadColorsFailureAction(payload) {
    return {
        type: LOAD_COLORS_FAILURE,
        payload
    }
}