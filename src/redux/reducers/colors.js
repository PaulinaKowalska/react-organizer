import {LOAD_COLORS, LOAD_COLORS_SUCCESS, LOAD_COLORS_FAILURE} from '../action-types';

export default function (state = {}, action) {
    switch (action.type) {
        case LOAD_COLORS: {
            return {
                ...state,
                loader: true
            }
        }
        case LOAD_COLORS_SUCCESS: {
            const {colors} = action.payload;
            return {
                ...state,
                colors,
                loader: false
            }
        }
        case LOAD_COLORS_FAILURE: {
            const {error} = action.payload;
            console.log(error.message);
            return {
                ...state,
                error: {
                    message: error.message
                },
                loader: false
            }
        }
        default: {
            return state;
        }
    }
}