import {createStore, applyMiddleware} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import reducersStore from './reducers/combine-reducers';
import sagas from './sagas/conbines-sagas';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(reducersStore, composeWithDevTools(applyMiddleware(sagaMiddleware)));

sagaMiddleware.run(sagas);

export default store;
