export default async function loadData() {
    try {
        return await fetch('http://localhost:8080/data')
            .then(res => res.json())
    }
    catch (err) {
        throw new Error("Cannot load data");
    }
}