//Import necessary libraries
import express from 'express';
import path from 'path';
import config from './config';
//Create server by express
const app = express();

//Serve folder with static files
app.use('/', express.static(path.join(process.cwd(), '/dist/client')));

//Serve static HTML file
app.get('/', function (req, res) {
    res.sendFile(path.resolve(process.cwd() + '/src/index.html'));
});

//Listening on port
app.listen(config.port, err => {
    if(err) console.error(err);
console.log(`Server is running on port ${config.port}...`);
});